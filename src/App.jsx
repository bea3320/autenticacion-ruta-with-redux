import { useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { checkSessionAsync } from './redux/user.slice';

import {
  RegisterForm,
  LoginForm,
  Navbar,
  Home,
  SecureRoute,
  ComponentePrueba,
} from "./components";
import "./App.scss";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    dispatch(checkSessionAsync())
  };


  return (
    <Router>
      <div className="app">
      <Navbar />
        <h1>REACT</h1>
        <h2>AUTENTICACIÓN && ROUTER</h2>
        <Switch>
          <Route
            exact
            path="/register"
            component={(props) => 
              <RegisterForm {...props} />
            }
          />
          <Route
            exact
            path="/login"
            component={(props) => <LoginForm  {...props} />}
          />
          <SecureRoute
            exact
            path="/componentePrueba"
            component={(props) => <ComponentePrueba {...props} />}
          />
          <Route exact path="/" component={Home} />
        </Switch>
      </div>
    </Router>
  );
};
export default App;
