import { Link, withRouter } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logoutUser } from '../../redux/user.slice';
import './Navbar.scss';

const Navbar = (props) => {
    const dispatch = useDispatch();
    const { user } = useSelector(state => state.user);

    return (
        <nav className="nav">
            <div>
                <Link to="/">
                    Upgrade Auth
                </Link>
            </div>
            {user && <div>
                <span className="nav__text">
                    Bienvenido de nuevo, {user.username}
                </span>
                <button onClick={() => dispatch(logoutUser())}>Logout</button>
            </div>}
        </nav>
    )
}

export default withRouter(Navbar);

