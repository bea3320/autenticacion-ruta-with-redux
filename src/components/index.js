import RegisterForm from "./RegisterForm/RegisterForm";
import LoginForm from "./LoginForm/LoginForm";
import Navbar from "./Navbar/Navbar";
import Home from "./Home/Home";
import SecureRoute from "./SecureRoute/SecureRoute";
import ComponentePrueba from "./ComponentePrueba/ComponentePrueba";

export {
    RegisterForm,
    LoginForm,
    Navbar,
    Home,
    SecureRoute,
    ComponentePrueba
};
