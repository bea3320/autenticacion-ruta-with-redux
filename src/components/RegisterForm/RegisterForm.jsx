import { useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { registerAsync } from '../../redux/user.slice';
import './RegisterForm.scss';
//1. crear un estado vacío con los campos que contendrá mi formulario
//2. crear el formulario html
//3. crear función para el inputChange
//4. crear función para enviar al formulario
//5. Llamar a la función que hace el fecth a nuestro servidor

const INITIAL_STATE = {
  //vemos que valores son los que están definidos en el archivo de node
  username: "",
  email: "",
  password: "",
};

const RegisterForm = (props) => {
  const [formFields, setFormFields] = useState(INITIAL_STATE);
  const {error} = useSelector(state => state.user);
  const dispatch = useDispatch();

  const handleFormSubmit = async (ev) => {
      // 1. Recopilar los datos (formFields);
      // 2. Enviar petición al back de autenticación.
      ev.preventDefault();
      dispatch(registerAsync(formFields))
      setFormFields(INITIAL_STATE);
  };

  const handleInputChange = (ev) => {
      const { name, value } = ev.target;

      setFormFields({ ...formFields, [name]: value });

      console.log(formFields);
  };

  return (
    <div className="register-form">
      <h3>Registro</h3>
      <form onSubmit={handleFormSubmit}>
        <label htmlFor="username">
          <p>Nombre de usuario</p>
          <input
            type="text"
            name="username"
            id="username"
            placeholder="Nombre de Usuario"
            onChange={handleInputChange}
            value={formFields.username}
          />
        </label>
        <label htmlFor="email">
          <p>Email</p>
          <input
            type="email"
            name="email"
            id="email"
            placeholder="Email"
            onChange={handleInputChange}
            value={formFields.email}
          />
        </label>
        <label htmlFor="password">
          <p>Contraseña</p>
          <input
            type="password"
            name="password"
            id="password"
            placeholder="Contraseña"
            onChange={handleInputChange}
            value={formFields.password}
          />
        </label>
        <div className="register-form__button">
          <button type="submit">Registrarme</button>
        </div>
      </form>
      {error && <div className="register-form__error">
        {error}
        </div>}
    </div>
  );
};

export default RegisterForm;
