import { Link } from "react-router-dom";

const Home = (props) => {

  return (
    <div>
      <h1>Bienvenido a nuestra web!</h1>

      <p>Aprende Autenticación y rutas con React</p>
      <ul>
        <li>
          <Link to="/login">Ir a Login</Link>
        </li>
        <li>
        <Link to="/register">Ir a Registro</Link>
        </li>
        <li>
          <Link to={{ pathname: "/componentePrueba"}}>Ir a componente prueba</Link>
        </li>
      </ul>
    </div>
  );
};

export default Home;



