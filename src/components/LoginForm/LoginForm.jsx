import {useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loginAsync } from '../../redux/user.slice';
import './LoginForm.scss';

const INITIAL_STATE = {
    email:'',
    password: '',
}

const LoginForm = (props) => {
    //creamos las variables de estado
    const [formData, setFormData] = useState(INITIAL_STATE);
    const { error } = useSelector(state => state.user);
    const dispatch = useDispatch();


    const handleFormSubmit = async ev => {
      ev.preventDefault();
      await dispatch(loginAsync(formData));
  };


    const handleInputChange = event => {
        const {name, value} = event.target;
        setFormData({...formData, [name]: value});
    };

    return(
        <div className="login-form">
        <h3>Login</h3>
        <form onSubmit={handleFormSubmit}>
        <label htmlFor="email">
          <p>Email</p>
          <input
            type="email"
            name="email"
            id="email"
            placeholder="Email"
            onChange={handleInputChange}
            value={formData.email}
          />
        </label>
        <label htmlFor="password">
          <p>Contraseña</p>
          <input
            type="password"
            name="password"
            id="password"
            placeholder="Contraseña"
            onChange={handleInputChange}
            value={formData.password}
          />
        </label>
        <div className="login-form__button">
          <button type="submit">Acceder</button>
        </div>
      </form>
      {error && <div className="login-form__error">
        {error}
      </div>}
        </div>
    )
}

export default LoginForm;