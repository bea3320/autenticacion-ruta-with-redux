import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import { useSelector } from 'react-redux';

const SecureRoute = (props) => {
    const {hasUser} = useSelector(state => state.user);

    if (hasUser === null) {
        //Aún no ha vuelto la petición
        return(<div>Cargando....</div>);
    }
    if(hasUser) {
        //tenemos usuario: true
        return(<Route {...props} />)
    }
    if(!hasUser) {
        //no hay usuario logueado
        return (<Redirect to="/login" />)
    }
};

export default SecureRoute;